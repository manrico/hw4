import static java.lang.Math.toIntExact;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction a = new Lfraction(12345678901234567L,1);
      System.out.println(a.compareTo(new Lfraction(12345678901234568L, 1)));
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0) {
         throw new RuntimeException("Denominator cant be zero");
      }
      this.numerator = a;
      this.denominator = b;
   }

   public Lfraction () {
      this.numerator = 0;
      this.denominator = 0;
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
     return this.numerator + "/" + this.denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      Lfraction other = (Lfraction) m;
      long a = other.getNumerator() * denominator;
      long b = other.getDenominator() * numerator;
      return a == b;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      final long prime = 31;
      int result = 1;
      result = toIntExact(prime * result + denominator);
      result = toIntExact(prime * result + numerator);
      return result;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      // First lets just add them together (by multiplying with each oters denominator and numerators.)
      long newNumerator = (this.numerator * m.denominator) + (m.numerator * this.denominator);
      long newDenominator = (this.denominator * m.denominator);
      return new Lfraction(newNumerator , newDenominator).reduce();
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long num1 = numerator * m.numerator;
      long num2 = denominator * m.denominator;
      return new Lfraction(num1, num2).reduce();
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (numerator == 0) {
          throw new RuntimeException("Unable to invert zero.");
      }
      if (numerator<0) {
         return new Lfraction(-denominator, -numerator);
      } else {
         return new Lfraction(denominator, numerator);
      }
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction((-1)*this.numerator, this.denominator).reduce();
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      long num1 = (numerator * m.denominator) - (m.numerator * denominator);
      long num2 = denominator * m.denominator;
      if (num1 == 0 && num2 == 0) {
         throw new RuntimeException("Division by zero.");
      }
      return new Lfraction(num1,num2).reduce();

   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.getDenominator() == 0 || this.denominator == 0) {
         throw new RuntimeException("Can't divide by zero!");
      }
      return new Lfraction(numerator * m.getDenominator(), denominator * m.getNumerator()).reduce();
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      if (this.equals(m)) {
         return 0;
      }
      else if (m.toDouble() < this.toDouble()) {
         return 1;
      }
      else {
         return -1;
      }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {

      return new Lfraction (this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return (long) Math.floor(this.toDouble());
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      Lfraction integerPart = new Lfraction(this.integerPart(), 1);
      return this.minus(integerPart);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
     return (double)numerator / denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f*d), d).reduce();
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] chunks = s.split("/");
      long a = Long.parseLong(chunks[0]);
      long b = Long.parseLong(chunks[1]);
      return new Lfraction(a, b);
   }

   // http://www.java67.com/2012/08/java-program-to-find-gcd-of-two-numbers.html
   // Reduces this Fraction by dividing both the numerator
   // and denominator by their greatest common divisor.
   private Lfraction reduce () {
      long gcd = gcd(this.getNumerator(), this.getDenominator());

      long newNumerator = this.getNumerator() / gcd;
      long newDenominator = this.getDenominator() / gcd;

      return new Lfraction(newNumerator, newDenominator);
   }

   // http://www.java67.com/2012/08/java-program-to-find-gcd-of-two-numbers.html
   // Computes and returns the greatest common divisor of two positive numbers
   // using Euclid's algorithm.
   private long gcd(long a, long b) {
      if (b == 0)
      {
         return a;
      }
      return gcd(b, a%b);
   }
}

